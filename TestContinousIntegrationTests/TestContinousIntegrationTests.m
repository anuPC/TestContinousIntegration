//
//  TestContinousIntegrationTests.m
//  TestContinousIntegrationTests
//
//  Created by DA-Administrator on 04/05/2017.
//  Copyright © 2017 DA-Administrator. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ViewController.h"

@interface TestContinousIntegrationTests : XCTestCase
@property (nonatomic) ViewController *vcToTest;
@end

@implementation TestContinousIntegrationTests

- (void)setUp {
    [super setUp];
    self.vcToTest = [[ViewController alloc] init];

    [self testValueForNewViewController];
//    [self testViewControllerView];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
//- (void) testViewControllerView {
//    // setup
//    ViewController *viewController = (ViewController *)[[UIApplication sharedApplication] delegate];
//   UIView *calcView     = viewController.view;
//
//    XCTAssertNotNil(calcView, @"Cannot find CalcView instance");
//    // no teardown needed
//}


- (void) testValueForNewViewController {
    NSString *originalString = @"Annapurna";
    NSString *reversedString = [self.vcToTest reverseString:originalString];
    
    NSString *expectedReversedString = @"anrupannA";
    XCTAssertEqualObjects(expectedReversedString, reversedString);
   }

@end
